Xam.iTextSharpLGPL
========================
This project is packaging the latest version of iTextSharp licensed with LGPL/MPL into a Xamarin ios/android plugin.
Solution is barely the union of two projects already porting iTextSharp 4.1.6 to monotouch:
iTextSharpLGPL-Monotouch https://github.com/JamieMellway/iTextSharpLGPL-Monotouch
iTextSharpLGPL-MonoForAndroid https://github.com/JamieMellway/iTextSharpLGPL-MonoForAndroid

Nuget package available here https://www.nuget.org/packages/Xam.iTextSharpLGPL/0.1.0

Licensing
=========
You have three license choices when it comes to iTextSharp: LGPL/MPL, AGPL, or a commercial license. The LGPL/MPL license is only an option with the older 4.1.6 version (used here). After that version, they switched to a dual AGPL/Commercial.


If you need a more recent version, you either have to make your project open-source or pay the license fee. 

Other Links
===========
iTextSharp-LGPL NuGet Package
http://nuget.org/packages/iTextSharp-LGPL

iTextSharp-4.1.6 From GitHub
https://github.com/itextsharper/iTextSharp-4.1.6

iTextSharp
http://itextpdf.com
